package kruskal.struct.tree;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import kruskal.struct.graph.Vertex;

/**
 *
 * @author Renã P. Souza
 */
public  class BinaryTree {
	
	private Map<Vertex<?>, Node> mapNodes;
	private List<Node> nodes;
	private Node lastNode;
	int size = 0;
	
	public BinaryTree(){
		mapNodes = new HashMap<>();
		nodes = new LinkedList<>();
	}
	
	public void makeSet(Vertex<Integer> i) {
		Node node = new Node(i);
		mapNodes.put(i, node);
		nodes.add(node);
		size++;
	}
	
	public  Node findCanonical(Vertex<Integer> v) {
		Node q = mapNodes.get(v);		
		while(q.getParent() != null)
			q = q.getParent();
		
		return q;
	}
	
	public  void union(Node x, Node y) {
		Node p = new Node(x, y);
		nodes.add(p);
		lastNode = p;
	}
	
	public  void doSomething(Vertex<Integer> x, Vertex<Integer> y) {
		//do nothing
	}

	public void print() {
		print(lastNode, 0);
	}
	
	private void print(Node p, int t){
		if(p == null)
			return;
		String toPrint =  "";
		for(int i = t; i > 0 ; i--)
			toPrint += "   ";
		
		
		toPrint+= p.val() != null ? "├── " + p.val().getLabel() : "└── ";
		System.out.println(toPrint);
		t++;
		print(p.getFirstChild() , t);
		print(p.getSecondChild(), t);
	}
	
}