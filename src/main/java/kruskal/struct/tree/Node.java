package kruskal.struct.tree;

import kruskal.struct.graph.Vertex;

/**
 *
 * @author Renã P. Souza
 */
public class Node {
	private Vertex<Integer> v;

	private Node parent, firstChild, secondChild;

	public Node(Vertex<Integer> v) {
		if (v == null)
			throw new NullPointerException("value can not be null");
		this.v = v;
	}

	public Node(Node firstChild, Node secondChild) {
		this.firstChild = firstChild;
		this.secondChild = secondChild;
		firstChild.setParent(this);
		secondChild.setParent(this);
	}

	void setParent(Node p) {
		parent = p;
	}

	Node getParent() {
		return parent;
	}
	
	public Node getFirstChild() {
		return firstChild;
	}
	
	public Node getSecondChild() {
		return secondChild;
	}

	public Vertex<Integer> val() {
		return v;
	}
}
