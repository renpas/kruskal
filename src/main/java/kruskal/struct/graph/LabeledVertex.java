package kruskal.struct.graph;

import java.util.Collection;

/**
 *
 * @author Renã P. Souza
 */
public interface LabeledVertex<T> extends Vertex<T> {
	
	String getLabel();
	Collection<Vertex<T>> getNodes();

}
