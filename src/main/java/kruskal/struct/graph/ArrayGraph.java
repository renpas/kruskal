package kruskal.struct.graph;

import static java.lang.System.out;

import java.util.ArrayList;

/**
 *
 * @author Renã P. Souza
 */
public class ArrayGraph<T> implements Graph<T>{
	
	int arcsNumber;
	int vertexNumber;
	ArrayList<Arc<T>> arcs;
	
	public ArrayGraph() {
		arcs = new ArrayList<>();
	}

	@Override
	public void insert(Vertex<T> v, Vertex<T> w) {
		arcs.add(new ArcImpl<T>(v, w));
	}
	
	
	public void insert(T v, T w) {
		this.insert(new SimpleVertex<T>(v), new SimpleVertex<T>(w));
	}

	@Override
	public void remove(Vertex<T> v, Vertex<T> w) {
		throw new RuntimeException("Not implemented");		
	}

	@Override
	public void show() {
		for(Arc<T> a : arcs){
			out.printf("%d-%d ", a.v().val(), a.w().val());
		}
		out.printf("\n");
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(Arc<T> a : arcs)
			sb.append(String.format("%d-%d ", a.v().val(), a.w().val()));
		return sb.toString();
	}

}
