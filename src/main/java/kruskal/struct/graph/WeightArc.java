package kruskal.struct.graph;

/**
 *
 * @author Renã P. Souza
 */
public interface WeightArc<T> extends Arc<T>{
	
	public double getWeight();

}
