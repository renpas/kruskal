package kruskal.struct.graph;

/**
 *
 * @author Renã P. Souza
 */
public class WeightedArc<T> implements WeightArc<T> {

	private Vertex<T> v, w;
	private int weight;

	public WeightedArc(Vertex<T> v, Vertex<T> w, int weight) {
		this.v = v;
		this.w = w;
		this.weight = weight;
		v.addArc(this);
		w.addArc(this);
	}
		
	public Vertex<T> v(){
		return v;
	}
	
	public Vertex<T> w(){
		return w;
	}

	@Override
	public Vertex<T> getOther(Vertex<T> vertex) {
		return v.equals(vertex) ? w : v;
	}

	@Override
	public double getWeight() {
		return weight;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof WeightArc<?>))
			return false;
		
		WeightArc<?> other = (WeightArc<?>) obj;
		
		return (other.v().equals(v) && other.w().equals(w) && other.getWeight() == weight);
	}
	
}
