package kruskal.struct.graph;

import java.util.Collection;

/**
 *
 * @author Renã P. Souza
 */
public interface Vertex<T> {

	public T val();
	
	public Collection<Arc<T>> arcs();
	public void addArc(Arc<T> arc);
	public String getLabel();
	
}
