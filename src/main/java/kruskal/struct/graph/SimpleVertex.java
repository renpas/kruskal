package kruskal.struct.graph;

import static java.util.Collections.unmodifiableCollection;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Renã P. Souza
 */
public class SimpleVertex<T> implements Vertex<T>{
	
	private T val;
	private Collection<Arc<T>> arcs;
	
	public SimpleVertex(T val){
		this.val = val;
		arcs = new ArrayList<>();
	}
	
	@Override
	public void addArc(Arc<T> arc) {
		arcs.add(arc);	
	}
	
	public T val(){
		return val;
	}
	
	public Collection<Arc<T>> arcs(){
		return unmodifiableCollection(arcs);
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return "Not labeld";
	}
	
}
