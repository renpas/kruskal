package kruskal.struct.graph;

import static java.util.Collections.unmodifiableCollection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Renã P. Souza
 */
public class LabeledVertexImpl<T> implements LabeledVertex<T> {
	
	private T val;
	private Collection<Arc<T>> arcs;
	private Set<Vertex<T>> nodes;
	private String label;
	
	public LabeledVertexImpl(T val, String label){
		this.val = val;
		arcs = new HashSet<>();
		this.label = label;
		nodes = new HashSet<>();
	}
	
	@Override
	public void addArc(Arc<T> arc) {
		Vertex<T> other = arc.v().equals(this) ? arc.w() : arc.v();
		if(nodes.contains(other))
			return;
		nodes.add(other);
		arcs.add(arc);
	}
	
	public T val(){
		return val;
	}
	
	@Override
	public String getLabel() {
		return label;
	}
	
	public Collection<Arc<T>> arcs(){
		return unmodifiableCollection(arcs);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof LabeledVertex))
			return super.equals(obj);
		
		LabeledVertex<?> other = (LabeledVertex<?>) obj;
			
		if(!label.equals(other.getLabel()))
			return false;
		
		return val.equals(other.val()); 
	}

	@Override
	public Collection<Vertex<T>> getNodes() {
		return nodes;
	}

	@Override
	public String toString() {
		return label;
	}
}
