package kruskal.struct.graph;

/**
 *
 * @author Renã P. Souza
 */
public interface Arc<T> {
	
	public Vertex<T> v();
	
	public Vertex<T> w();	
	
	public Vertex<T> getOther(Vertex<T> vertex);

}
