package kruskal.struct.graph;

import static java.lang.String.format;
import static java.lang.System.out;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Renã P. Souza
 */
public class WeightGraph<T> {
	
	int arcsNumber;
	int vertexNumber;
	Set<WeightArc<T>> arcs;
	Set<LabeledVertex<T>> vertexs;
	Map<String, LabeledVertex<T>> labels;
	
	public WeightGraph() {
		arcs = new HashSet<>();
		labels = new HashMap<>();
		vertexs = new HashSet<>();
	}
	
	public void insert(LabeledVertex<T> v){
		findOrAddVertex(v);
	}

	public void insert(LabeledVertex<T> v, LabeledVertex<T> w, int weight) {
		v = findOrAddVertex(v);
		w = findOrAddVertex(w);
		
		addArc(v, w, weight);
	}
	
	private void addArc(LabeledVertex<T> v, LabeledVertex<T> w, int weight){
		if(v.getNodes().contains(w))
			return;
		else
			arcs.add(new WeightedArc<T>(v, w, weight));
	}
	
	private LabeledVertex<T> findOrAddVertex(LabeledVertex<T> v){
		LabeledVertex<T> w = labels.get(v.getLabel());
		if(w != null)
			return w;
		
		labels.put(v.getLabel(), v);
		vertexs.add(v);
		return v;
	}
	
	public void show() {
		vertexs.stream().forEach(this::printVertex);
		out.printf("\n");
	}
	
	public void showLabels(){
		vertexs.stream().sorted((v, w) -> v.getLabel().compareTo(w.getLabel()))
						.forEach(this::printLabel);
		out.printf("\n");
	}
	
	private void printLabel(LabeledVertex<T> v){
		out.printf(v.getLabel() + " : ");
		v.arcs().stream().forEach((a) -> out.print(a.getOther(v).getLabel()+ " "));
		out.print("\n");
	}
	
	private void printVertex(LabeledVertex<T> v){
		String node = format("%s(%s) :", v.val().toString(), v.getLabel());
		StringBuilder sb = new StringBuilder(node);
		for(Arc<T> a :v.arcs()){
			sb.append(a.getOther(v).val().toString()+" ");
		}
		out.println(sb.toString());
	}
	
	public List<WeightArc<T>> getArcsOrdened(){
		List<WeightArc<T>> arcsList = arcs.stream()
									  .sorted(this::compareArc)
									  .collect(toList());
		return arcsList;
	}
	
	public Collection<? extends Vertex<T>> getNodes(){
		return vertexs;
	}
	
	private int compareArc(WeightArc<T> a, WeightArc<T> b){
		return Double.compare(a.getWeight(), b.getWeight());
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(Arc<T> a : arcs)
			sb.append(String.format("%d-%d ", a.v().val(), a.w().val()));
		return sb.toString();
	}
}
