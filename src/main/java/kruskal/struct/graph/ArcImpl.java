package kruskal.struct.graph;

/**
 *
 * @author Renã P. Souza
 */
public class ArcImpl<T> implements Arc<T>{
	
	private Vertex<T> v, w;

	public ArcImpl(Vertex<T> v, Vertex<T> w) {
		this.v = v;
		this.w = w;
		v.addArc(this);
		w.addArc(this);
	}
	
	public Vertex<T> v(){
		return v;
	}
	
	public Vertex<T> w(){
		return w;
	}

	@Override
	public Vertex<T> getOther(Vertex<T> vertex) {
		return v.equals(vertex) ? w : v;
	}

}
