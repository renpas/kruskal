package kruskal.struct.graph;

/**
 *
 * @author Renã P. Souza
 */
public interface Graph<T> {
	
	void insert(Vertex<T> v, Vertex<T> w);
	void remove(Vertex<T> v, Vertex<T> w);
	void show();
	
}
