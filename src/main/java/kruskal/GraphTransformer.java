package kruskal;

import static java.lang.String.format;

import kruskal.struct.graph.LabeledVertex;
import kruskal.struct.graph.LabeledVertexImpl;
import kruskal.struct.graph.WeightGraph;
import mmlib4j.images.GrayScaleImage;

/**
 *
 * @author Renã P. Souza
 */
public class GraphTransformer {
	
	WeightGraph<Integer> graph;
	private GrayScaleImage img;
	
	public GraphTransformer(GrayScaleImage img) {
		this.img = img;
		graph = new WeightGraph<>();
	}
	
	public WeightGraph<Integer> fromImageToGraph(){
		for(int y = 0; y < img.getHeight(); y++){
			for(int x =0; x < img.getWidth(); x++){
				int valor = img.getPixel(x,y);
				LabeledVertex<Integer> vertex = vert(valor, x, y); 
				graph.insert(vertex);
				insertNeighboars(vertex, x, y, img.getHeight(), img.getWidth());
			}
		}
		return graph;
	}
	
	private void insertNeighboars(LabeledVertex<Integer> vert, int x, int y, int maxX, int maxY){
//		if(x > 0 && x < maxX - 1){
//			
//			graph.insert(vert, vert(img.getPixel(x + 1, y), x+1, y));
//		}
//		
//		if(x > 0)
//			graph.insert(vert, vert(img.getPixel(x - 1, y), x-1, y));
//		
//		if(y > 0 && y < maxY - 1)
//			graph.insert(vert, vert(img.getPixel(x, y + 1), x, y + 1));
//		
//		if(y > 0)
//			graph.insert(vert, vert(img.getPixel(x, y - 1), x, y - 1));
	}
	
	private LabeledVertex<Integer> vert(int val, int x, int y){
		return new LabeledVertexImpl<Integer>(val, format("[%d,%d]", x,y));
	}

}
