package kruskal;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;

import java.util.Date;

import kruskal.struct.graph.LabeledVertex;
import kruskal.struct.graph.LabeledVertexImpl;
import kruskal.struct.graph.WeightGraph;
import mmlib4j.images.GrayScaleImage;
import mmlib4j.utils.ImageBuilder;

/**
 *
 * @author Renã P. Souza
 */
public class GraphUtils {
	
	public static void main(String[] args) throws InterruptedException {
		GrayScaleImage img = ImageBuilder.openGrayImage();
		long i = currentTimeMillis();
		System.out.println(new Date());
		WeightGraph<Integer> graph = new GraphUtils().fromImageToGraph(img);
		System.out.println("Total: " + (currentTimeMillis() - i) / 1000);
		Thread.sleep(30000);
		graph.show();
		System.out.println("Total: " + (currentTimeMillis() - i) / 1000);
		System.out.println(new Date(currentTimeMillis() - i));
	}

	WeightGraph<Integer> graph;
	private GrayScaleImage img;
	
	public WeightGraph<Integer> fromImageToGraph(GrayScaleImage img){
		graph = new WeightGraph<>();
		this.img = img;
		for(int y = 0; y < img.getHeight(); y++){
			for(int x =0; x < img.getWidth(); x++){
				int valor = img.getPixel(x,y);
				LabeledVertex<Integer> vertex = vert(valor, x, y); 
				graph.insert(vertex);
				insertNeighboars(vertex, x, y, img.getHeight(), img.getWidth());
			}
		}
		return graph;
	}
	
	private void insertNeighboars(LabeledVertex<Integer> vert, int x, int y, int maxX, int maxY){
//		if(x > 0 && x < maxX - 1)
//			graph.insert(vert, vert(img.getPixel(x + 1, y), x+1, y));
//		
//		if(x > 0)
//			graph.insert(vert, vert(img.getPixel(x - 1, y), x-1, y));
//		
//		if(y > 0 && y < maxY - 1)
//			graph.insert(vert, vert(img.getPixel(x, y + 1), x, y + 1));
//		
//		if(y > 0)
//			graph.insert(vert, vert(img.getPixel(x, y - 1), x, y - 1));
	}
	
	public LabeledVertex<Integer> vert(int val, int x, int y){
		return new LabeledVertexImpl<Integer>(val, format("[%d,%d]", x,y));
	}
	
}
