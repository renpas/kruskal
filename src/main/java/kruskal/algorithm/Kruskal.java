package kruskal.algorithm;

import java.util.Set;

/**
 *
 * @author Renã P. Souza
 */
public interface Kruskal {
	
	/**
	 * add a new element q1 to the collection Q, provided that the element q1 
	 * does not already belongs to a set in Q
	 * @param q1
	 */
	void MakeSet(Object q1);
	/**
	 * @param q1
	 * @return the canonical element of the set in Q which con- tains q1. 
	 */
	Object findCanonical(Object q1);
	
	/**
	 * let Q1 and Q2 be the two sets in Q whose canonical elements are q1 and q2
	 * respectively (q1 and q2 must be different). Both sets are removed
	 * from Q, their union Q3 = Q1 ∪ Q2 is added to Q and a canonical element 
	 * for Q3 is selected and returned.
	 * @param q1
	 * @param q2
	 */
	void union(Set<?> q1, Set<?> q2);

}
