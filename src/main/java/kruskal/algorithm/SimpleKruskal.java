package kruskal.algorithm;

import kruskal.struct.graph.Vertex;
import kruskal.struct.graph.WeightArc;
import kruskal.struct.graph.WeightGraph;
import kruskal.struct.tree.BinaryTree;
import kruskal.struct.tree.Node;

/**
 *
 * @author Renã P. Souza
 */
public class SimpleKruskal {
	
	BinaryTree Q;
	Node MST[][];
	WeightGraph<Integer> g;
	
	public SimpleKruskal(WeightGraph<Integer> g) {
		this.g = g;
		this.Q = new BinaryTree();
		MST = new Node[5000][2];
		doKruskal();
	}
	
	private void doKruskal() {
		int e = 0;

		for (Vertex<Integer> i : g.getNodes()) {
			Q.makeSet(i);
		}

		for (WeightArc<Integer> c : g.getArcsOrdened()) {
			Node cx = Q.findCanonical(c.v());
			Node cy = Q.findCanonical(c.w());
			if (!cx.equals(cy)) {
				Q.union(cx, cy);
				MST[e][0] = cx;
				MST[e][1] = cy;
				e++;
			}

		}
	}
	
	public void printTree(){
		Q.print();
	}
}
