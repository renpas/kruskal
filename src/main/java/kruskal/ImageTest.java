package kruskal;

import static java.lang.String.format;

import kruskal.struct.graph.LabeledVertexImpl;
import kruskal.struct.graph.WeightGraph;
import mmlib4j.images.GrayScaleImage;
import mmlib4j.utils.ImageBuilder;

/**
 *
 * @author Renã P. Souza
 */
public class ImageTest {
	
	public static void main(String[] args) {
		GrayScaleImage img = ImageBuilder.openGrayImage();
		
		WeightGraph<Integer> graph = new WeightGraph<>();
		for(int y = 0; y < img.getHeight(); y++){
			for(int x =0; x < img.getWidth(); x++){
				int valor = img.getPixel(x,y);
				graph.insert(new LabeledVertexImpl<Integer>(valor, format("[%d,%d]", x,y)));
			}
		}
		
		graph.show();
	}

}
