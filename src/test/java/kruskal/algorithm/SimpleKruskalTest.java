package kruskal.algorithm;

import kruskal.struct.graph.LabeledVertex;
import kruskal.struct.graph.LabeledVertexImpl;
import kruskal.struct.graph.WeightGraph;

/**
 *
 * @author Renã P. Souza
 */
public class SimpleKruskalTest {

	public static void main(String[] args) {
//		getImg().showLabels();
		
		SimpleKruskal kruskal = new SimpleKruskal(getImg());
		kruskal.printTree();
//		System.out.println(Arrays.asList(kruskal.MST));
	}
	
	public static WeightGraph<Integer> getImg(){
		WeightGraph<Integer> graph = new WeightGraph<>();
		
		LabeledVertex<Integer> a,b,c,d,e,f,g,h;
		
		a = getLabel("a", 1);
		b = getLabel("b", 2);
		c = getLabel("c", 2);
		d = getLabel("d", 2);
		e = getLabel("e", 2);
		f = getLabel("f", 2);
		g = getLabel("g", 2);
		h = getLabel("h", 2);

		graph.insert(a, b, 2);
		graph.insert(b, c, 2);
		graph.insert(c, d, 0);
		graph.insert(a, e, 0);
		graph.insert(e, f, 1);
		graph.insert(f, b, 2);
		graph.insert(f, g, 2);
		graph.insert(g, c, 3);
		graph.insert(g, h, 0);
		graph.insert(h, d, 1);
		
		return graph;
	}
	
	public static LabeledVertex<Integer> getLabel(String label, int value){
		return new LabeledVertexImpl<Integer>(value, label);
	}
}
