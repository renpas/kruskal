package kruskal.struct.graph;

import static junit.framework.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

/**
 *
 * @author Renã P. Souza
 */
public class WeightGraphTest {
	
	@Test
	public void orderTest(){
		WeightGraph<Integer> graph = new WeightGraph<>();
		graph.insert(label(1), label(3), 2);
		graph.insert(label(3), label(4), 1);
		graph.insert(label(2));
		graph.insert(label(2), label(5), 3);
		graph.insert(label(6));
		
		List<WeightArc<Integer>> arcs = graph.getArcsOrdened();
	
		WeightArc<Integer> arc0 = arcs.get(0);
		WeightArc<Integer> arc1 = arcs.get(1);
		WeightArc<Integer> arc2 = arcs.get(2);
		
		assertEquals(1d, arc0.getWeight());
		assertEquals(2d, arc1.getWeight());
		assertEquals(3d, arc2.getWeight());
	}
	
	@Test
	public void createArcTest(){
		WeightGraph<Integer> graph = new WeightGraph<>();
		graph.insert(label(1), label(2), 1);
		
		WeightArc<Integer> arc = graph.getArcsOrdened().get(0);
		int v = arc.v().val();
		int w = arc.w().val();
		assertEquals(1, v);
		assertEquals(2, w);
	}
	
	
	public LabeledVertex<Integer> label(int x){
		return new LabeledVertexImpl<Integer>(x, x + "");
	}

}
