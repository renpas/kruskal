package kruskal.struct.graph;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

import kruskal.struct.graph.ArrayGraph;

/**
 *
 * @author Renã P. Souza
 */
public class ArrayGraphTest {

	@Test	
	public void insertTest(){
		ArrayGraph<Integer> graph = new ArrayGraph<Integer>();
		graph.insert(1,0);
		graph.insert(2,0);
		graph.insert(3,0);
		assertEquals("1-0 2-0 3-0 ", graph.toString());
	}
	
}
